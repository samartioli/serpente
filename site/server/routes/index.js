/*
 * GET home page.
 */

exports.index = function(req, res) {
	res.render('index', {
		title: 'SNAKES ON A (2D) PLANE',
		noCanvas: 'This Browser does not support canvas. Try Google Chrome.'
	});
};

/*
 * GET multi player page.
 */

exports.multi = function(req, res) {
	res.render('multi', {
		title: 'SNAKES ON A (2D) PLANE',
		noCanvas: 'This Browser does not support canvas. Try Google Chrome.'
	});
};


/*
 * POST multi player page.
 */

exports.multi_post = function(roomIdGenerator, redis, redisClient) {
	function multi_post(req, res)	 {
	    var roomId = roomIdGenerator.next();
	    redisClient.set('room:' + roomId, 'multi-player room', redis.print)
	    res.set('Location', '/multi?' + roomId)
	    res.send(302);
	}

	return multi_post;
};