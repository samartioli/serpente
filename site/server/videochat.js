exports.register = function(io, redisClient) {
    io.sockets.on('connection', function (socket) {
        joinRoom(socket, redisClient);
        establishRoomHooks(socket, redisClient);
    });
}

function establishRoomHooks(socket, redisClient) {
    socket.get('roomId', function(errName, roomId) {
        socket.get('nickName', function(errName, nickName) {
            // handle error conditions
            handleMessage(socket, roomId, nickName);
            handleEmoticon(socket, roomId, nickName);
            handleControl(socket, roomId, nickName);
            handleClientDisconnection(socket, redisClient, roomId, nickName);
            handleRoomList(socket, redisClient, roomId);
            handleWebRtc(socket, roomId, nickName);
        });
    });
}

function handleMessage(socket, roomId, nickName) {
    socket.on('send_message', function (msg) {
        console.log('received message: ' + JSON.stringify(msg));
        var sendMsg = createMessageForSending(msg, nickName, nickName);
        socket.broadcast.to(roomId).emit('receive_message', sendMsg);
        socket.emit('receive_message', sendMsg);
    });
}

function handleEmoticon(socket, roomId, nickName) {
    socket.on('send_emoticon', function (msg) {
        console.log('received emoticon: ' + JSON.stringify(msg));
        var sendMsg = createMessageForSending(msg, nickName, nickName);
        socket.broadcast.to(roomId).emit('receive_emoticon', sendMsg);
        socket.emit('receive_emoticon', sendMsg);
    });
}

function handleControl(socket, roomId, nickName) {
    socket.on('send_control', function (msg) {
        console.log('received room_list request');
        var sendMsg = createMessageForSending(msg, nickName, nickName);
        socket.broadcast.to(roomId).emit('receive_control', sendMsg);
        socket.emit('receive_control', sendMsg);
    });
}

function handleWebRtc(socket, roomId, nickName) {
    socket.on('candidate_send', function (msg) {
        console.log("received candidate:" +JSON.stringify(msg));
        socket.broadcast.to(roomId).emit('candidate_receive', msg);
    });

    socket.on('sdp_send', function (msg) {
        console.log("received sdp:" + msg);
        socket.broadcast.to(roomId).emit('sdp_receive', msg);
    });
}

function handleClientDisconnection(socket, redisClient, roomId, nickName) {
    socket.on('disconnect', function() {
        console.log("disconnected:" + socket.id);
        socket.broadcast.to(roomId).emit('room_leave', {user: {id: 'vidinno', name: 'vidinno'}, body: {data: nickName}});
        redisClient.lrem('room:' + roomId + ':members', 1, nickName);
    });
}

function joinRoom(socket, redisClient) {
    var roomId = socket.handshake.query.room
    var nickName = socket.handshake.query.nickName
    if(!roomId) {
        console.log("Missing room for: " + socket.id);
        socket.emit('error_message', 'room information missing');
        return;
    }
    // set the nick-name to anonymous
    if(!nickName) {
        nickName = 'anonymous';
    }
    socket.set('roomId', roomId, function() {
        socket.set('nickName', nickName, function() {
            // handler error conditions
            socket.join(socket.handshake.query.room);
            console.log(socket.id + " joined room: " + socket.handshake.query.room);
            redisClient.rpush('room:' + roomId + ':members', nickName);
            //redisClient.rpush('room:' + roomId + ':members', nickName);

            redisClient.get('room:' + roomId + ':url', function(err, url){
                console.log('Initializing ' + socket.id + ' with url: ' + url);
                //socket.emit('video_url', {user: {id: 'vidinno', name: 'vidinno'}, body: {data: url}});
                socket.broadcast.to(roomId).emit('room_join', {user: {id: 'vidinno', name: 'vidinno'}, body: {data: nickName}});
            });
        });
    });
}

function handleRoomList(socket, redisClient, roomId) {
    socket.on('room_list', function (msg) {
        redisClient.lrange('room:' + roomId + ':members', 0, -1, function(err, members) {
            socket.emit('room_list', {user: {id: 'vidinno', name: 'vidinno'}, body: {data: members}});
        });
    });
}

function createMessageForSending(receivedMessage, userId, nickName) {
    return {user : {id: userId, name: nickName}, body: receivedMessage.body};
}