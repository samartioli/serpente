var argv = require('optimist')
    .options('p', {
        alias : 'path',
        default : 'dev',
        describe: 'path to deploy [dev]|publish'
    })
    .options('r', {
        alias : 'redisip',
        default : '127.0.0.1',
        describe: 'redis ip address'
    })
    .argv
;

var public_path = 'public/' + argv.path;
var redis_ip = argv.redisip;


/**
 * Module dependencies.
 */

var express = require('express')
    , routes = require('./routes')
    , user = require('./routes/user')
    , http = require('http')
    , path = require('path')
    , videoChat = require('./videochat')
    , redis  = require('redis')
    , connectRedis = require('connect-redis')(express)
    , io = require('socket.io')
    , sioExpress = require('socket.io-express')
    , stochator = require('stochator');

;

console.log(routes);

var cookieParser = express.cookieParser('secret');

var redisClient = redis.createClient();
var redisStore = new connectRedis({
    client: redisClient
});

var authFunction = sioExpress.createAuthFunction(
    cookieParser,
    redisStore,
    'serpente.sid'
);

var roomIdGenerator = new stochator({
    kind: 'integer',
    min: 100,
    max: 999
}, {
    kind: 'integer',
    min: 1000,
    max: 9999
}, function(values) {
    return values[0] + "-" + values[1];
});

var app = express(),
    server = http.createServer(app),
    sio = io.listen(server);
    sio.set('authorization', authFunction);
    sio.set('log level', 1);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(cookieParser);
app.use(express.session({
        secret: 'secret',
        key: 'serpente.sid',
        store: redisStore
    }));

app.use(app.router);
app.use(require('less-middleware')({ src: path.join(__dirname, public_path) }));

// console.log(path.join(__dirname, public_path));

app.use(express.static(path.join(__dirname, public_path)));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

app.get('/multi', routes.multi);
app.post('/multi', routes.multi_post(roomIdGenerator, redis, redisClient));



server.listen(app.get('port'), function(){
	console.log("INFO: Serving up: " + public_path);
 	console.log('INFO: KEEP server listening on port ' + app.get('port'));
});



videoChat.register(sio, redisClient);


// sio.sockets.on('connection', function (socket) {

//         console.log("connection established");
        
//         socket.emit('server_hello', { ciao: 'mondo' });

//         socket.on('client_hello', function (msg) {
//             console.log(msg);
//         });

// });


