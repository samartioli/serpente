playerGenerator = new Stochator({
    kind: 'integer',
    min: 1000000000,
    max: 9999999999
});

var player = playerGenerator.next();
var players = {};

//;(function() {

    var size = 50,
        timer = 100,
        container = document.getElementById('snake-container'),
        apple = {
            x: 0,
            y: 0
        },
        timeoutID
        ;

    var createPlayer = function() {

        players[player] = {
            snake: [{
                    x: 0,
                    y: 0
                }
            ],
            color: 'black',
            xDir: 0,
            yDir: 1,
            which: 'p1',
            score: 0

        }

        // players[2] = {
        //     snake: [{
        //             x: size-1,
        //             y: 0
        //         }
        //     ],
        //     color: 'green',
        //     xDir: 0,
        //     yDir: 1,
        //     number: 1
        // }

    };

    var init = function () {

        console.log('init');

        createPlayer();

        createGrid();
        initKeyBindings();
    	//resetGame();

    };

    var resetGame = function () {

        var p = players[player];

        p.snake = [{
                x: 0,
                y: 0
            }
        ];

        p.xDir = 0;
        p.yDir = 1;
        p.score = 0;
        updateScore(p);

        $('.snake-block').removeClass('red black');
        setInitialSnake();
        setApple();

    }

    var start = function() {

    	console.log('start')
        resetGame();
        timeoutID = setTimeout(runLoop, timer);

    };

    var runLoop = function() {

        ateAppleCheck();
        collisionCheck();
        clearScreen();
        drawSnakes();
        moveSnakes();

    };

    var collisionCheck = function() {

        var p = players[player];


    };

    var drawSnakes = function ( ) {
        var i
            , snake = players[player].snake;
            snakeLength = snake.length;

        for (i = 0; i < snakeLength; i++) {
            var block = document.getElementById('block-' + snake[i].y + '-' + snake[i].x);
            block.className += ' black';
        }

    };

    var clearScreen = function() {
        $('.black').removeClass('black');
    };

    var ateAppleCheck = function() {

        var p = players[player];
        var snake = p.snake;

        if (snake[0].x == apple.x && snake[0].y == apple.y) {
            
            console.log('ate apple');
            //Clear Apple
            $('#block-' + apple.y + '-' + apple.x).removeClass('red');

            p.score += 10;
            updateScore(p);

            var snakeLength = snake.length;
            
            for (i=0; i<3; i++) {
                snake.push({
                    x: snake[(snakeLength-1)].x,
                    y: snake[(snakeLength-1)].y
                });
            }

            setApple();

        };

    }

    var updateScore = function(p) {
        $('#' + p.which + 'score').html(p.score);
    };

    var moveSnakes = function() {

        var p = players[player];
        var snake = p.snake;

        // Consolidated x and y check
        var head = {
            x: snake[0].x + p.xDir,
            y: snake[0].y + p.yDir
        }

        var nextBlock = '#block-' + (snake[0].y + p.yDir) + '-' + 
            (snake[0].x + p.xDir);

        //console.log(nextBlock);

        if (snake[0].x + p.xDir >= 0 && snake[0].x + p.xDir < size &&
            snake[0].y + p.yDir >= 0 && snake[0].y + p.yDir < size &&
            !$(nextBlock).hasClass(p.color)) {
                snake.unshift(head);
                snake.pop();
        } else {
            die();
            return;
        }

        timeoutID = setTimeout(runLoop, timer);

    };

    // game over, son
    var die = function() {
        alert('ur dead');
        window.clearTimeout(timeoutID);
        resetGame();
    };

    var initKeyBindings = function() {

        var p = players[player];

        Mousetrap.bind('up', function(e) {
            e.preventDefault();
            if (p.yDir !== 1) {
                p.yDir = -1;
                p.xDir = 0;
            }
        });

        Mousetrap.bind('down', function(e) {
            e.preventDefault();
            if (p.yDir !== -1) {
                p.yDir = 1;
                p.xDir = 0;
            }
        });

        Mousetrap.bind('left', function(e) {
            e.preventDefault();
            if (p.xDir !== 1) {
                p.yDir = 0;
                p.xDir = -1;
            }
        });

        Mousetrap.bind('right', function(e) {
            e.preventDefault();
            if (p.xDir !== -1) {
                p.yDir = 0;
                p.xDir = 1;
            }
        });

        Mousetrap.bind('s', function(e) {
            e.preventDefault();
            start();
        });

        Mousetrap.bind('esc', function(e) {
            e.preventDefault();
            die();
        });

    }

    var setInitialSnake = function () {

        var p
            ,snake
            ;

        for (p in players) {
            console.log('set initial' + players[p].snake);
            snake = players[p].snake;
            document.getElementById('block-' + snake[0].y + '-' + snake[0].x).className += ' ' + players[p].color;

        }

    };

    var setApple = function () {

        apple.y = Math.floor(Math.random() * size);
        apple.x = Math.floor(Math.random() * size);
        document.getElementById('block-' + apple.y + '-' + apple.x).className += ' red';

    };

    var createGrid = function() {

        var x,y;

        var outerFrag = document.createDocumentFragment();

        // create the grid
        // rows
        for (y = 0; y < size; y++) {
            var frag = document.createDocumentFragment();
            // columns
            for (x = 0; x < size; x++) {
                var div = document.createElement('div');
                div.className = 'snake-block';
                div.id = 'block-' + y + '-' + x;
                frag.appendChild(div);
            }

            frag.appendChild(document.createElement('br'));
            outerFrag.appendChild(frag);
        }

        container.appendChild(outerFrag);

    };

    var setSpeed = function(speed) {

        switch(speed) {
            case 'Slow' :
                timer = 150;
                break;

            case 'Medium' :
                timer = 100;
                break;

            case 'Fast' :
                timer = 50;
                break;

        };

    };




//})();