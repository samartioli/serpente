var socket
	, host = 'http://localhost:3000'
	;

$(function() {

	// Parse Room ID from URL and Initiate WebSocket
	href = window.location.href;
	room = href.replace(/.*\/multi\?(.*)\/?/,"$1");
	socket = io.connect(host, {query:'room='+room+'&'+'nickName='+player});

	// socket.on('video_url', function (msg) {
	//     video_url = msg.body.data;
	//     _init_video();
	// });
	
	socket.emit('room_list');


	socket.on('room_list', function (msg) {
	    console.log('room_list: ' + JSON.stringify(msg));
	    console.log('users in room: ' + msg.body.data.length);
	    
	    // users_in_room = '';
	    // for (var i=0; i<msg.body.data.length; i++) {
	    //     users_in_room = users_in_room + msg.body.data[i] + ' ';
	    // }
	    
	});


	socket.on('room_join', function (msg) {
	    console.log('room_join: ' + JSON.stringify(msg));
	    socket.emit('room_list');
	});

	
});
