'use strict';

var socket;
var host = 'http://localhost:3000';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {

  	console.log('mc1');

  	socket = io.connect(host);

  	socket.on('server_hello', function (msg) {
  		console.log(msg)
  	});

  	socket.emit('client_hello', { hello: 'world' });


  }])
  .controller('MyCtrl2', [function() {

  	console.log('mc2');

  }]);