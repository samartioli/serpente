SERPENTE
========

SERPENTE is built from [KEEP](https://bitbucket.org/samartioli/keep)

### Goal:
Build a snake game (play an example here if you're not familiar: http://www.snakegame.net/digisnake.htm)

### Rules:
1. Snake runs into edge - GAME OVER
2. Snake runs into itself - GAME OVER
3. Snake eats an "apple" - add 3 blocks to snake

### Bonus:
1. Implement a scoring mechanism (e.g. 10 points per apple eaten)
2. Adjustable speed (slow, medium, fast)
3. Make it look nice (use your own judgement)

Nice things to know:    
Using Mousetrap library for key bindings, documentation here: http://craig.is/killing/mice    
Feel free to use whatever libraries necessary, but be able to explain why you're using them.


## Note:

Change in socket.io-express.js

	//if(err || !session || !session.auth || !session.auth.loggedIn){
	if(err || !session){


## Running 

    nodemon --debug -e ".js|.html|.css|.jade" app.js